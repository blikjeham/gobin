package main

import (
	"fmt"
)

func (field *Field) checkDouble(i1, i2, i3 int) error {
	value := field.cells[i1].value
	inverse := ((value - 1) ^ 1) + 1
	if value != 0 && field.cells[i2].value == value {
		if field.cells[i3].value == 0 {
			field.cells[i3].value = inverse
		} else if field.cells[i3].value != inverse {
			return fmt.Errorf("Inconsistent double %d.%d, %d", i1, i2, value)
		}
	}
	return nil
}

// check for horizontal doubles
func (field *Field) solveDoubleHorizontal(y int) error {
	for x := 0; x < field.width-2; x++ {
		i1 := field.index(x, y)
		i2 := i1 + 1
		i3 := i1 + 2

		// Forward
		if err := field.checkDouble(i1, i2, i3); err != nil {
			return err
		}

		// Backward
		if err := field.checkDouble(i3, i2, i1); err != nil {
			return err
		}

		// Split
		if err := field.checkDouble(i1, i3, i2); err != nil {
			return err
		}
	}
	return nil
}

// Check for vertical doubles
func (field *Field) solveDoubleVertical(x int) error {
	for y := 0; y < field.height-2; y++ {
		i1 := field.index(x, y)
		i2 := i1 + field.width
		i3 := i1 + (field.width * 2)

		// Forward
		if err := field.checkDouble(i1, i2, i3); err != nil {
			return err
		}

		// Backward
		if err := field.checkDouble(i3, i2, i1); err != nil {
			return err
		}

		// Split
		if err := field.checkDouble(i1, i3, i2); err != nil {
			return err
		}
	}
	return nil
}

func (field *Field) solveDoubles() error {
	for y := 0; y < field.height; y++ {
		if err := field.solveDoubleHorizontal(y); err != nil {
			field.print()
			return err
		}
	}
	for x := 0; x < field.width; x++ {
		if err := field.solveDoubleVertical(x); err != nil {
			field.print()
			return err
		}
	}
	return nil
}
