package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

// binary cell
type cell struct {
	value int
}

// binary field
type Field struct {
	width, height int
	cells         []cell
}

// total size of the field
func (field *Field) size() int {
	return field.width * field.height
}

// get the index based on x and y coordinates
func (field *Field) index(x, y int) int {
	return field.width*y + x
}

func (field *Field) printHeader() {
	for x := 0; x < field.width; x++ {
		fmt.Print("-")
	}
	fmt.Print("\n")
}

// print the entire field.
func (field *Field) print() {
	field.printHeader()
	for y := 0; y < field.height; y++ {
		for x := 0; x < field.width; x++ {
			index := field.index(x, y)
			if field.cells[index].value != 0 {
				fmt.Printf("%d", field.cells[index].value-1)
			} else {
				fmt.Print("_")
			}

		}
		fmt.Print("\n")
	}
	field.printHeader()
}

// Return how many empty cells are left
func (field *Field) left() int {
	count := 0
	for i := 0; i < field.size(); i++ {
		if field.cells[i].value == 0 {
			count++
		}
	}
	return count
}

func readSize(field *Field, data string) (int, error) {
	i := strings.Index(data, "x")
	var err error
	field.width, err = strconv.Atoi(string(data[:i]))
	if err != nil {
		return 0, err
	}
	j := strings.Index(data, "\n")
	field.height, err = strconv.Atoi(string(data[i+1 : j]))
	if err != nil {
		return 0, err
	}
	return i + j, err
}

func readField(field *Field, filename string) error {
	raw, err := os.ReadFile(filename)
	if err != nil {
		return err
	}
	data := string(raw)

	var skip int
	if skip, err = readSize(field, data); err != nil {
		return err
	}
	fmt.Printf("field size %d\n", field.size())
	field.cells = make([]cell, field.size())
	i := 0
	for skip < len(data) {
		number := string(data[skip])
		if number == "0" || number == "1" {
			value, _ := strconv.Atoi(number)
			field.cells[i].value = value + 1
			i++
		} else if number == "." {
			field.cells[i].value = 0
			i++
		}
		skip++
	}
	return nil
}
