package main

import (
	"fmt"
	"log"
)

func main() {
	// TODO: get filename from commandline
	field := Field{}
	if err := readField(&field, "field.bin"); err != nil {
		log.Fatal(err)
	}
	field.print()
	fmt.Println("Start solving field")
	left := field.left()
	previous := 0
	for left != previous {
		fmt.Printf("Open cells left: %d\n", left)
		previous = left
		if err := field.solveDoubles(); err != nil {
			fmt.Println(err)
			break
		}
		left = field.left()
		field.print()
	}
	fmt.Printf("Final result: %d\n", left)
	field.print()
}
